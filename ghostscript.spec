%global _hardened_build 1
# override the default location of documentation or license files
# in 'ghostscript' instead of in 'libgs'
%global _docdir_fmt     %{name}
# download version
%global version_short   %(echo "%{version}" | tr -d '.')
# Obtain the location of Google Droid fonts directory
%global google_droid_fontpath %%(dirname $(fc-list : file | grep "DroidSansFallback"))

Name:             ghostscript
Version:          9.52
Release:          1
Summary:          An interpreter for PostScript and PDF files
License:          AGPLv3+
URL:              https://ghostscript.com/
Source0:          https://github.com/ArtifexSoftware/ghostpdl-downloads/releases/download/gs%{version_short}/ghostscript-%{version}.tar.xz


Patch0000: ghostscript-9.23-100-run-dvipdf-securely.patch
BuildRequires:    automake gcc
BuildRequires:    adobe-mappings-cmap-devel adobe-mappings-pdf-devel
BuildRequires:    google-droid-sans-fonts urw-base35-fonts-devel
BuildRequires:    cups-devel dbus-devel fontconfig-devel
BuildRequires:    lcms2-devel libidn-devel libijs-devel libjpeg-turbo-devel
BuildRequires:    libpng-devel libpaper-devel libtiff-devel openjpeg2-devel
BuildRequires:    zlib-devel gtk3-devel libXt-devel
BuildRequires:    jbig2dec-devel >= 0.16

Requires:         adobe-mappings-cmap
Requires:         adobe-mappings-cmap-lang
Requires:         adobe-mappings-pdf
Requires:         google-droid-sans-fonts
Requires:         urw-base35-fonts

Obsoletes:      %{name}-doc < %{version}-%{release}
Obsoletes:      %{name}-x11 < %{version}-%{release}
Obsoletes:      %{name}-gtk < %{version}-%{release}
Obsoletes:      %{name}-tools-printing < %{version}-%{release}
Obsoletes:      %{name}-tools-fonts < %{version}-%{release}
Obsoletes:      libgs < %{version}-%{release}
Provides:       %{name}-doc
Provides:       %{name}-x11
Provides:       %{name}-gtk
Provides:       %{name}-tools-printing
Provides:       %{name}-tools-fonts
Provides:       libgs
Provides:       %{name}-core

%description
Ghostscript is an interpreter for PostScript™ and Portable Document Format (PDF) files.
Ghostscript consists of a PostScript interpreter layer, and a graphics library.

%package devel
Summary:          Development files for Ghostscript's library
Requires:         %{name} = %{version}-%{release}

Obsoletes:        libgs-devel < %{version}-%{release}
Provides:         libgs-devel

%description devel
This package contains development files for %{name}.

%package        help
Summary:        Documents for %{name}
Buildarch:      noarch
Requires:       man info
Requires:       %{name} = %{version}-%{release}

Obsoletes:      %{name}-doc < %{version}-%{release}
Provides:       %{name}-doc

%description help
Man pages and other related documents for %{name}.

%package tools-dvipdf
Summary:          Ghostscript's 'dvipdf' utility
Requires:         %{name} = %{version}-%{release}
Requires:         texlive-dvips

%description tools-dvipdf
This package provides the utility 'dvipdf' for converting of TeX DVI files into
PDF files using Ghostscript and dvips

%prep
%autosetup -n %{name}-%{version} -p1

# Libraries that we already have packaged(see Build Requirements):
rm -rf cups/libs freetype ijs jbig2dec jpeg lcms2* libpng openjpeg tiff zlib
rm -rf windows

%build
%configure --enable-dynamic --disable-compile-inits --without-versioned-path \
           --with-fontpath="%{urw_base35_fontpath}:%{google_droid_fontpath}:%{_datadir}/%{name}/conf.d/"
%make_build so

%install
# to install necessary files without 'make_install'
make DESTDIR=%{buildroot} soinstall

# rename to 'gs' binary.
mv -f %{buildroot}%{_bindir}/{gsc,gs}

# remove files
rm -f %{buildroot}%{_bindir}/{lprsetup.sh,unix-lpr.sh}
rm -f %{buildroot}%{_docdir}/%{name}/{AUTHORS,COPYING,*.tex,*.hlp,*.txt}
rm -f %{buildroot}%{_datadir}/%{name}/doc

# move some files into html/
install -m 0755 -d %{buildroot}%{_docdir}/%{name}/html
cp doc/gsdoc.el %{buildroot}%{_docdir}/%{name}/
mv -f %{buildroot}%{_docdir}/%{name}/{*.htm*,*.el,html}

# create symlink
ln -s %{_bindir}/gs %{buildroot}%{_bindir}/ghostscript
ln -s %{_mandir}/man1/gs.1 %{buildroot}%{_mandir}/man1/ghostscript.1

# use the symlinks where possible.
ln -fs %{google_droid_fontpath}/DroidSansFallback.ttf %{buildroot}%{_datadir}/%{name}/Resource/CIDFSubst/DroidSansFallback.ttf

for font in $(basename --multiple %{buildroot}%{_datadir}/%{name}/Resource/Font/*); do
  ln -fs %{urw_base35_fontpath}/${font}.t1 %{buildroot}%{_datadir}/%{name}/Resource/Font/${font}
done

# create symlink for each of the CMap files in Ghostscript's Resources/CMap folder.
for file in $(basename --multiple %{buildroot}%{_datadir}/%{name}/Resource/CMap/*); do
  find %{adobe_mappings_rootpath} -type f -name ${file} -exec ln -fs {} %{buildroot}%{_datadir}/%{name}/Resource/CMap/${file} \;
done

install -m 0755 -d %{buildroot}%{_datadir}/%{name}/conf.d/

%check
make check

%pre

%preun

%post

%postun

%files
%defattr(-,root,root)
%license LICENSE doc/COPYING
%{_datadir}/%{name}/
%dir %{_datadir}/%{name}/conf.d/
%{_bindir}/gs
%{_bindir}/gsnd
%{_bindir}/ghostscript
%{_bindir}/eps2*
%{_bindir}/pdf2*
%{_bindir}/ps2*
%{_bindir}/gsx
%{_bindir}/gsbj
%{_bindir}/gsdj
%{_bindir}/gsdj500
%{_bindir}/gslj
%{_bindir}/gslp
%{_bindir}/pphs
%{_bindir}/pf2afm
%{_bindir}/pfbtopfa
%{_bindir}/printafm
%{_libdir}/libgs.so.*
%{_libdir}/%{name}/

%files devel
%{_libdir}/libgs.so
%{_includedir}/%{name}/

%files help
%{_mandir}/man1/*
%lang(de) %{_mandir}/de/man1/*
%doc %{_docdir}/%{name}/

%files tools-dvipdf
%{_bindir}/dvipdf

%changelog
* Sun Jun 28 2020 wangchen <wangchen137@huawei.com> - 9.52-1
- Type:requirement
- ID:NA
- SUG:NA
- DESC:update ghostscript to 9.52

* Wed Mar 18 2020 openEuler Buildteam <buildteam@openeuler.org> - 9.27-8
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:fix autosetup patch

* Wed Mar 18 2020 openEuler Buildteam <buildteam@openeuler.org> - 9.27-7
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:add run dvipdf securely

* Tue Jan 7 2020 chengquan<chengquan3@huawei.com> - 9.27-6
- Type:CVE
- ID:NA
- SUG:NA
- DESC:fix CVE-2019-14869

* Tue Jan 7 2020 chengquan<chengquan3@huawei.com> - 9.27-5
- Type:CVE
- ID:NA
- SUG:NA
- DESC:remove useless patch

* Fri Jan 3 2020 wangxiao<wangxiao65@huawei.com> - 9.27-4
- Type:CVE
- ID:NA
- SUG:NA
- DESC:fix CVE-2019-14811 CVE-2019-14812 CVE-2019-14813 CVE-2019-14817

* Mon Sep 23 2019 openEuler Buildteam <buildteam@openeuler.org> - 9.27-3
- fix CVE-2019-10216 and modify requires

* Mon Sep 23 2019 openEuler Buildteam <buildteam@openeuler.org> - 9.27-2
- Add subpackage tools-dvipdf

* Thu Sep 19 2019 openEuler Buildteam <buildteam@openeuler.org> - 9.27-1
- Package init
